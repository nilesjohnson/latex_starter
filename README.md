LATEX STARTER AND ADVANCED DEMO
--

version 2.0  2015-06


DESCRIPTION
-----------

This repository contains a latex starter template for first-time users
of latex.  It also contains an "advanced" demo giving package
recommendations for moderate-to-advanced latex users.


LICENSE
--
This work is in the public domain.


CHANGELOG
--

A version of the latex starter was first written for REU students at
the University of Chicago.  Niles Johnson added the advanced demo in
June 2015.
