\documentclass{amsart}
\usepackage{hyperref}

%%
%% commutative diagrams with tikz
%%
\usepackage{tikz}
\usetikzlibrary{decorations.markings,decorations.pathmorphing}
\usetikzlibrary{arrows}
\usepackage{tikz-cd} % we use some features from this package 

%%
%% tikz can provide arrowheads that match the default font
%% but they don't always look good; adjust as you like
%%
%\tikzset{/tikz/commutative diagrams/arrow style=math font} % matching arrowheads
\tikzset{/tikz/commutative diagrams/arrow style=tikz,>=stealth} % almost matching arrowheads; these look better in pdf than the others

%%
%% styles for mathmode, objects, and arrows
%%
\tikzset{mm/.style={execute at begin node=$\displaystyle, execute at end node=$}}
\tikzset{tikzob/.style={commutative diagrams/every diagram, every cell}}
\tikzset{tikzar/.style={commutative diagrams/.cd, every arrow, every label, font={\small}}}

% style of squiggly arrows
\tikzset{tikzsquiggle/.style={decorate, decoration={
    snake,
    segment length=8pt,
    amplitude=.9pt,post=lineto,
    post length=2pt}}}

% for an arrow that crosses another arrow
\tikzset{cross line/.style={preaction={draw=white, -, line width=6pt}}}




\title{Diagrams with tikz}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}

This document demonstrates how to draw commutative diagrams with
tikz.  For rectangular diagrams, the matrix-style input of tikzcd
suffices, but for more complex diagrams it's useful to position nodes
by coordinates rather than \& tabs.

\noindent
Here's the basic idea for this system:
\begin{itemize}
\item Objects are positioned by coordinates and given labels.
\item Arrows connect objects referenced by their labels; thus when you
  adjust objects, the arrows automatically adjust.
\item Scale is controlled by independent parameters, so the entire
  diagram can be adjusted as necessary.
\end{itemize}

\noindent
And here's an example together with annotated code that produces it.
\[
  \begin{tikzpicture}[x=25mm,y=20mm] % this option sets x and y scales
    
    % draw the objects; use options tikzob and mm
    % to automatically set appropriate spacing
    % and ensure mathmode
    %
    % nodes are labeled so you don't have to
    % retype the coordinates for start/end points of arrows
    \draw[tikzob,mm]
    (0,0) node (a) {(xe)y}
    (1,0) node (b) {x(ey)}
    (-.5,-1) node (c) {xy}
    (1.5,-1) node (d) {xy};

    
    % next draw arrows (paths) to/from nodes labeled above
    % use options tikzar and mm to set arrow format and
    % ensure mathmode
    \path[tikzar,mm] %arrows
    (a) edge node {\alpha} (b)
    (c) edge node {r\,\mathrm{id}} (a)
    (c) edge[swap] node {\mathrm{id}} (d)
    (b) edge node {\mathrm{id} \, l} (d);


    % if 2-cells are needed, add them at the appropriate
    % positions
    \draw[tikzob,mm]
    (0.5,-0.5) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\mu};
  \end{tikzpicture}
\]
\begin{verbatim}
  \begin{tikzpicture}[x=25mm,y=20mm] % this option sets x and y scales
    
    % draw the objects; use options tikzob and mm
    % to automatically set appropriate spacing
    % and ensure mathmode
    %
    % nodes are labeled so you don't have to
    % retype the coordinates for start/end points of arrows
    \draw[tikzob,mm]
    (0,0) node (a) {(xe)y}
    (1,0) node (b) {x(ey)}
    (-.5,-1) node (c) {xy}
    (1.5,-1) node (d) {xy};

    
    % next draw arrows (paths) to/from nodes labeled above
    % use options tikzar and mm to set arrow format and
    % ensure mathmode
    \path[tikzar,mm] %arrows
    (a) edge node {\alpha} (b)
    (c) edge node {r\,\mathrm{id}} (a)
    (c) edge[swap] node {\mathrm{id}} (d)
    (b) edge node {\mathrm{id} \, l} (d);


    % if 2-cells are needed, add them at the appropriate
    % positions
    \draw[tikzob,mm]
    (0.5,-0.5) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\mu};
  \end{tikzpicture}
\end{verbatim}

To use this, you need the tikz styles defined in the preamble of this
document.  You also need to load some other tikz packages, and tikz-cd
(to get spacing of nodes right).  It's around a dozen lines of code
all together, and it's all in this preamble.


\section{More diagrams}

Here are some more examples for you to copy/paste and modify!  The
rest of these just use features that are already built in to tikz, and
show you how to use them for things one often wants to do with
diagrams.  If you develop any other useful examples, send them to
Niles or (bonus) just send a pull request to \url{https://gitlab.com/nilesjohnson/latex_starter.git}!

  \[
  \begin{tikzpicture}[x=35mm,y=25mm,baseline={(0,16mm)}]
    \draw[tikzob,mm] %objects
    (0,1) node (sax) {s.(a,x)}
    (1,1) node (tax) {t.(a,x)}
    (.5,0) node (by) {(b,y)};
    \path[tikzar,mm] %arrows
    (sax) edge[bend left=25] node {q.1} (tax) 
    (sax) edge[bend right=25,swap] node {p.1} (tax) 
    (sax) edge[swap] node {(\alpha,\phi)} (by)
    (tax) edge node {(\beta,\psi)} (by);
    \draw[tikzob,mm]
    (.5,.5) node[rotate=35,font=\Large] {\Rightarrow}
    (.5,1) node[rotate=90,font=\Large] {\Rightarrow}
      node[right] {\Theta};
  \end{tikzpicture}
  \ =\ 
  \begin{tikzpicture}[x=35mm,y=25mm,baseline={(0,16mm)}]
    \draw[tikzob,mm] %objects
    (0,1) node (sax) {s.(a,x)}
    (1,1) node (tax) {t.(a,x)}
    (.5,0) node (by) {(b,y)};
    \path[tikzar,mm] %arrows
    (sax) edge[bend left=25] node {q.1} (tax) 
    (sax) edge[swap] node {(\alpha,\phi)} (by)
    (tax) edge node {(\beta,\psi)} (by);
    \draw[tikzob,mm]
    (.5,.65) node[rotate=35,font=\Large] {\Rightarrow};
  \end{tikzpicture}
  \]

Here's another example which takes advantage of using polar
coordinates instead of cartesian coordinates to position vertices.

  \[
  \begin{tikzpicture}[x=15mm,y=15mm,baseline=(-60:1)]
    \draw[tikzob,mm] %objects
    (0,0) node (a) {F(c)}
      (-90:.55) node[rotate=-60,font=\small] {\Downarrow}
    (-120:1) node (b) {F(c')}
    (-60:1) node (c) {\sigma_i}
      ++(90:.55) node[font=\small] {\Downarrow}
    (0:1) node (d) {\sigma_j};
    \path[tikzar,mm] %arrows
    (a) edge (b)
    (b) edge (c)
    (c) edge (d)
    (a) edge (d)
    (a) edge (c);    
  \end{tikzpicture}
  =
  \begin{tikzpicture}[x=15mm,y=15mm,baseline=(-60:1)]
    \draw[tikzob,mm] %objects
    (0,0) node (a) {F(c)}
    (-60:1) node (b) {F(c')}
      +(90:.55) node[font=\small] {\Downarrow}
    ++(0:1) node (c) {\sigma_i}
    (0:1) node (d) {\sigma_j}
      ++(-90:.55) node[rotate=60,font=\small] {\Downarrow};
    \path[tikzar,mm] %arrows
    (a) edge (b)
    (b) edge (c)
    (c) edge (d)
    (a) edge (d)
    (b) edge (d);
  \end{tikzpicture}
  \]



Here's another example showing different arrow styles.

\[
\begin{tikzpicture}[x=20mm,y=18mm]
  \draw[tikzob,mm]
  (0,1) node (q) {[q]}
  (0,-1) node (p) {[p]}
  (1,0) node (qp) {[q]*[p]}
  (2,1) node (C) {\mathcal{C}}
  (2,-1) node (D) {\mathcal{D}}
  (-1,0) node (x) {[0]} % extra vertex
  ;
  \path[tikzar,mm]
  (x) edge[bend right=10] (C) 
  (x) edge (q)
  (x) edge (p)
  (q) edge[tikzsquiggle] node[above=.8mm] {\omega} (C)
  (p) edge[tikzsquiggle] node[below=.8mm] {\sigma} (D)
  (qp) edge[tikzsquiggle] node {\delta} (D)
  (q) edge[cross line] (qp)
  (p) edge (qp)
  (C) edge node {F} (D)
  ;
\end{tikzpicture}
\]


\newcommand{\la}{\lambda}
\newcommand{\al}{\alpha}
\newcommand{\id}{\mathrm{id}}
In a SMB: invertible modifications $\pi$, $\mu$, $\la$, $\rho$ as follows, \[
  \begin{tikzpicture}[x=25mm,y=20mm]
    \draw[tikzob,mm] %objects
    (0,0) node (0) {((xy)z)w}
    (1,1) node (1) {(x(yz))w}
    (3,1) node (2) {x((yz)w)}
    (4,0) node (3) {x(y(zw))}
    (2,-1) node (4) {(xy)(zw)};
    \path[tikzar,mm] %arrows
    (0) edge node {\al \,\id} (1)
    (1) edge node {\al} (2)
    (2) edge node {\id\,\al} (3)
    (0) edge[swap] node {\al} (4)
    (4) edge[swap] node {\al} (3);
    \draw[tikzob,mm]
    (2,0) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\pi};
  \end{tikzpicture}
  \]
    \[
  \begin{tikzpicture}[x=25mm,y=20mm]
    \draw[tikzob,mm] %objects
    (0,0) node (0) {(xe)y}
    (1,0) node (1) {x(ey)}
    (-.5,-1) node (2) {xy}
    (1.5,-1) node (3) {xy};
    \path[tikzar,mm] %arrows
    (0) edge node {\al} (1)
    (2) edge node {r\,\id} (0)
    (2) edge[swap] node {\id} (3)
    (1) edge node {\id \, l} (3);
    \draw[tikzob,mm]
    (0.5,-0.5) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\mu};
  \end{tikzpicture}
  \]
    \[
  \begin{tikzpicture}[x=45mm,y=20mm]
    \draw[tikzob,mm] %objects
    (0,0) node (0) {(ex)y}
    (1,0) node (1) {xy}
    (.5,-1) node (2) {e(xy)};
    \path[tikzar,mm] %arrows
    (0) edge node {l\,\id} (1)
    (0) edge[swap] node {\al} (2)
    (2) edge[swap] node {l} (1);
    \draw[tikzob,mm]
    (0.5,-0.5) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\la};
  \end{tikzpicture}
  \qquad
    \begin{tikzpicture}[x=45mm,y=20mm]
    \draw[tikzob,mm] %objects
    (0,0) node (0) {xy}
    (1,0) node (1) {x(ye)}
    (.5,-1) node (2) {(xy)e};
    \path[tikzar,mm] %arrows
    (0) edge node {\id\, r} (1)
    (0) edge[swap] node {r} (2)
    (2) edge[swap] node {\al} (1);
    \draw[tikzob,mm]
    (0.5,-0.5) node[rotate=270,font=\Large] {\Rightarrow}
    ++(3mm,0mm) node {\rho};
  \end{tikzpicture}
  \]


The following diagram appears at the end of the SHH2 paper, and has
some custom bending of arrows.

\newcommand{\can}{\kappa}
\newcommand{\epz}{\varepsilon}
\[
\begin{tikzpicture}[x=30mm,y=25mm]
  \draw[tikzob,mm] %objects
  (0,3) node (pgm) {.}
  (0,2) node (wsbo) {.}
  (-2,2) node (o) {.}
  (2,2) node (wsc) {.}
  (-1.2,1) node (wno) {.}
  (1.2,1) node (wsd) {.}
  (0,0) node (sm2) {.}
  ;
  \path[->,auto,mm] %arrows;  have to use this instead of tikzar, 
                    % for some reason
  (pgm) edge[swap, bend right=15] node {} (o)
  (pgm) edge[swap, bend right=25] node {} (wno)
  (pgm) edge node {} (wsbo)
  (pgm) edge[bend left=25] node {} (wsd)
  (pgm) edge[bend left=15] node {WS(LB-)} (wsc)
  (o.240) edge[swap, bend right=35, in=210, looseness=1.4] node {\can^*} (sm2)
  (o) edge[swap] node {\epz^*} (wno)
  (wsbo) edge node {h^*} (wno)
  (wsbo) edge[swap] node {p_1^*} (wsd)
  (wsc) edge node {p_2^*} (wsd)
  (wsc.300) edge[bend left=35, in=150, looseness=1.4] node {\nu^*} (sm2)
  (wno) edge node {} (sm2)
  (wsd) edge[swap] node {} (sm2)
  ;
  \draw[tikzob,mm]
  (-1.25,2.15) node[rotate=200,font={\Large}] {\Rightarrow} node[above]
  {\epz}
  (-0.55,2.1) node[rotate=-20,font={\Large}] {\Rightarrow} node[above]
  {h}
  (0.55,2.1) node[rotate=200,font={\Large}] {\Rightarrow} node[above]
  {WS(\xi)}
  (1.25,2.15) node[rotate=-20,font={\Large}] {\Rightarrow} node[above]
  {WS(\al)}
  (0,1) node[font={\Large}] {=}
  (-1.2,.5) node[rotate=45,font={\Large}] {=}
  (1.2,.5) node[rotate=-45,font={\Large}] {=}
  ;
\end{tikzpicture}
\]


Some other diagram, illustrating \emph{scope[shift]}, to put one
diagram inside another.

\newcommand{\iso}{\cong}
\newcommand{\ol}[1]{\overline{#1}}
\newcommand{\om}{\omega}
  \[
  \begin{tikzpicture}[x=20mm,y=20mm]
    \begin{scope}
    \draw[tikzob,mm] %objects
    (0,.5) node (01) { }
    (1,.5) node (11) { }
    (2,.5) node (21) { }
    (0,-.5) node (00) { }
    (2,-.5) node (20) { }
    ;
    \draw[tikzar,mm] %morphisms
    (01) edge node{t^*} (11)
    (11) edge[bend left] node{Tf} (21)
    (11) edge[swap, bend right] node{Tg} (21)
    (01) edge[swap] node{\al\: 1} (00)
    (21) edge node{\beta} (20)
    (00) edge[swap,bend right] node{g} (20)
    ;
    \draw[tikzob,mm]
    (1,-.05) node[rotate=30] {\iso} node[right=.25pc] {\ol{g}_2}
    (1.5,.5) node {\Downarrow T\om}    
    ;
    \end{scope}  
    \draw[tikzob,mm] (2.5,0) node {=};
    \begin{scope}[shift={(3,0)}]
    \draw[tikzob,mm] %objects
    (0,.5) node (01) { }
    (1,.5) node (11) { }
    (2,.5) node (21) { }
    (0,-.5) node (00) { }
    (2,-.5) node (20) { }
    ;
    \draw[tikzar,mm] %morphisms
    (01) edge node{t^*} (11)
    (11) edge[bend left] node{Tf} (21)
    (01) edge[swap] node{\al\: 1} (00)
    (21) edge node{\beta} (20)
    (00) edge[bend left] node{f} (20)
    (00) edge[swap, bend right] node{g} (20)
    ;
    \draw[tikzob,mm]
    (1.2,.2) node[rotate=30] {\iso} node[right=.25pc] {\ol{f}_2}
    (1,-.5) node {\Downarrow T\om}
    ;
    \end{scope}
  \end{tikzpicture}
  \]


\end{document}
