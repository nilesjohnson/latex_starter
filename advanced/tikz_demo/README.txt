COMMUTATIVE DIAGRAMS WITH TIKZ

These examples use `demo.sty`, which is really just a few macros for extracting the functionality of `tikz-cd`, but without having to use the matrix format for arranging nodes.
