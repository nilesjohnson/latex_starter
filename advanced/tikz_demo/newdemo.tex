\documentclass[10pt]{article}

\usepackage{fouriernc} % Fourier fonts instead of Computer Modern
\usepackage[margin=1cm]{geometry}
\usepackage{multicol}
\pagestyle{empty}




%%
%% commutative diagrams with tikz
%%
%% based on tikz-cd, but improved so that we 
%% don't have to use the matrix format
%%
\usepackage{tikz, tikz-cd}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.markings,decorations.pathmorphing}
\usetikzlibrary{arrows}
\usetikzlibrary{matrix}
%\tikzset{/tikz/commutative diagrams/arrow style=math font} % matching arrowheads
\tikzset{/tikz/commutative diagrams/arrow style=tikz,>=stealth} %almost matching arrowheads; look better in pdf
%\tikzset{mm/.style={execute at begin node=$\displaystyle, execute at end node=$}}
%\tikzset{tikzob/.style={commutative diagrams/every diagram, every cell}}
%\tikzset{tikzar/.style={commutative diagrams/.cd, every arrow, every label, font={\small}}}
\tikzset{0cell/.style={commutative diagrams/every diagram,
    every cell,
    execute at begin node=$\displaystyle, execute at end node=$, 
    font={\small},
  }
}
\tikzset{1cell/.style={commutative diagrams/.cd,
    every arrow,
    every label,
    execute at begin node=$\displaystyle, execute at end node=$, 
  }
}
\tikzset{2cell/.style={commutative diagrams/every diagram,
    every cell,
    execute at begin node=$\displaystyle, execute at end node=$, 
  }
}
\tikzset{equal/.style={commutative diagrams/equal}}
\tikzset{tikzsquiggle/.style={decorate, decoration={
      snake,
      segment length=8pt,
      amplitude=.9pt,post=lineto,
      post length=2pt}}}
\tikzset{cross line/.style={preaction={draw=white, -, line width=6pt}}}
\tikzset{
  between/.style args={#1 and #2 at #3}{
    at = ($(#1)!#3!(#2)$)
  },
}


%%
%% macros for the demo
%%
\newcommand{\ol}[1]{\overline{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}
\newcommand{\dar}{\downarrow}
\newcommand{\cn}{\colon}





\begin{document}


Given a 2-cell $\gamma\cn u \to u'$ in $B$, we have 1-cells in $F\dar y$
given by $(\wt{u},\theta_{\wt{u}})$ and $(\wt{u'},\theta_{\wt{u'}})$:
\[
  \begin{tikzpicture}[x=15mm,y=15mm,baseline={(x).base}]
    \draw[0cell] %objects
    (0,0) node (y) {y}
    (120:2) node (a) {F(\wt{x})}
    (120:1) node (x) {x}
    (60:2) node (b) {F(\wt{y})}
    ;
    \path[1cell] %arrows
    (a) edge[swap] node {f_{\wt{x}}} (x)
    (x) edge[swap] node {u} (y)
    (b) edge node {f_{\wt{y}}} (y)
    (a) edge node (T) {F(\wt{u})} (b)
    ;
    \draw[2cell] %2cells
    node[between=y and T at .65, rotate=30, font=\Large] (A) {\Rightarrow}
    (A) node[below right] {\theta_{\wt{u}}}
    ;
  \end{tikzpicture}
  \qquad \mathrm{ and } \qquad
  \begin{tikzpicture}[x=15mm,y=15mm,baseline={(x).base}]
    \draw[0cell] %objects
    (0,0) node (y) {y.}
    (120:2) node (a) {F(\wt{x})}
    (120:1) node (x) {x}
    (60:2) node (b) {F(\wt{y})}
    ;
    \path[1cell] %arrows
    (a) edge[swap] node {f_{\wt{x}}} (x)
    (x) edge[swap] node {u'} (y)
    (b) edge node {f_{\wt{y}}} (y)
    (a) edge node (T) {F(\wt{u'})} (b)
    ;
    \draw[2cell] %2cells
    node[between=y and T at .65, rotate=30, font=\Large] (A) {\Rightarrow}
    (A) node[below right] {\theta_{\wt{u'}}}
    ;
  \end{tikzpicture}
\]
Moreover, we have a third 1-cell in $(1_{\wt{x}}, \ol{\gamma})$ where
$\ol{\gamma}$ denotes the 2-cell obtained from the pasting below:
\[
  \begin{tikzpicture}[x=20mm,y=25mm]
    \draw[0cell] %objects
    (0,-.25) node (y) {y.}
    (120:2) node (a) {F(\wt{x})}
    (90:.6) node (x) {x}
    (60:2) node (b) {F(\wt{x})}
    % (60:1) node (x2) {x}
    ;
    \path[1cell] %arrows
    (a) edge[swap] node {f_{\wt{x}}} (x)
    (b) edge node {f_{\wt{x}}} (x) 
    (x) edge[swap, bend right] node {u} (y)
    (x) edge[bend left] node {u'} (y) 
    % (x) edge[swap] node (1) {1_x} (x2)
    (a) edge[bend left] node (T) {F(1_{\wt{x}})} (b) 
    (a) edge[swap, bend right] node (B) {1_{F(\wt{x})}} (b) 
    ;
    \draw[2cell] %2cells
    node[between=x and B at .6, rotate=30, font=\Large] (A) {\Rightarrow}
    (A) ++(.02,0) node[below] {\ell}
    node[between=y and x at .45, font=\Large] (B) {\Rightarrow}
    (B) node[above] {\gamma}
    node[between=a and b at .5, rotate=90, font=\Large] (C) {\Rightarrow}
    (C) node[right] {F_{\wt{x}}}
    ;
  \end{tikzpicture}
\]

These three 1-cells form a triangle in $F \dar y$ and therefore
there is a unique 2-cell filling the diagram below in $F \dar y$.
\[
  \begin{tikzpicture}[x=15mm,y=15mm]
    \draw[0cell] %objects
    (0,0) node (y) {(\wt{y},f_{\wt{y}})}
    (90:2) node (x) {(\wt{x}, u f_{\wt{x}})}
    (30:2) node (x') {(\wt{x}, u' f_{\wt{x}})}
    ;
    \path[1cell] %arrows
    (x) edge[swap] node (L) {(\wt{u},\theta_{\wt{u}})} (y)
    (x) edge node {(1_{\wt{x}}, \ol{\gamma})} (x')
    (x') edge node {(\wt{u'},\theta_{\wt{u'}})} (y)
    ;
    \draw[2cell] %2cells
    node[between=L and x' at .5, font=\Large,shift={(0,-.1)}] (A) {\Rightarrow}
    (A) node[above] {(\wt{\gamma})}
    ;
  \end{tikzpicture}
\]
Thus we have the following equality in $B$.
\[
  \begin{tikzpicture}[x=25mm,y=25mm,baseline={(eq).base}]
    \def\w{1.5} % horizontal shift between pastings
    \def\h{-1.5} % vertical shift of pastings
    \def\m{.5} % margin around pastings
    \draw[font=\Large] (\w/2+\m,0) node (eq) {=}; 
    \newcommand{\boundary}{
      \draw[0cell] %objects 
      (0,-.25) node (y) {y}
      (120:2) node (a) {F(\wt{x})}
      (135:.6) node (x) {x}
      (60:2) node (b) {F(\wt{y})}
      ;
      \path[1cell] %arrows
      (a) edge[swap] node {f_{\wt{x}}} (x)
      (b) edge node (fy) {f_{\wt{y}}} (y) 
      (x) edge[swap] node {u} (y)
      % (x) edge[swap] node (1) {1_x} (x2)
      (a) edge[bend left] node (T) {F(\wt{u'} 1_{\wt{x}})} (b) 
      ;
    }
    \begin{scope}[shift={(0,\h/2)}]
      \boundary
      \path[1cell] %arrows
      (a) edge[swap, bend right] node (B) {F(\wt{u})} (b) 
      ;
      \draw[2cell] %objects
      node[between=a and b at .5, rotate=90, font=\Large] (C) {\Rightarrow}
      (C) node[right] {F(\wt{\gamma})}
      ;
    \end{scope}      
    \begin{scope}[shift={(\w+\m+\m,\h/2)}]
      \boundary
      \draw[0cell] %objects
      (90:1.25) node (a2) {F(\wt{x})}
      ;
      \path[1cell] %arrows
      % (x) edge[bend left] node {u'} (y) 
      (a) edge[swap, bend left] node {F(1_{\wt{x}})} (a2)
      (a2) edge[swap, bend left] node {F(\wt{u'})} (b)
      (a2) edge node {} (y)
      ;
      \draw[2cell] %objects
      node[between=x and a2 at .5, shift={(-.15,.175)}, rotate=60, font=\Large] (AA) {\Rightarrow}
      (AA) node[below, shift={(.05,-.05)}] {\ol{\gamma}}
      node[between=fy and a2 at .5, shift={(.05,.1)}, rotate=60, font=\Large] (BB) {\Rightarrow}
      (BB) node[below, shift={(.05,-.05)}] {\theta_{\wt{u'}}}
      node[between=a2 and T at .55, shift={(-.075,0)}, rotate=90, font=\Large] (CC) {\Rightarrow}
      (CC) node[right] {F_{\wt{u'},1_{\wt{x}}}}
      ;
    \end{scope}
  \end{tikzpicture}
\]




\end{document}

